import React, { useEffect } from 'react';
import logo from './logo.svg';
import './App.css';



/**
 * 判断是否在tauri桌面端环境，否则在web端使用tauriAPI会报错
 * @returns 
 */
export const handleIsTauri = () => {
  return Boolean(
    typeof window !== 'undefined' &&
      window !== undefined &&
      (window as any).__TAURI_IPC__ !== undefined
  );
};

/**
 * 显示窗口
 */
const showWindow = async ()=> {
  const appWindow = (await import('@tauri-apps/api/window')).appWindow
  appWindow.show();
}

function App() {
  useEffect(()=>{
    if (handleIsTauri()) {
      showWindow();
    }
  },[])

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload2.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
